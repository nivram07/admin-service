package com.cmpe295.iot.admin.rest;

import com.cmpe295.iot.admin.model.Video;
import com.cmpe295.iot.admin.service.VideoService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class VideoController {

  private static final Logger LOGGER = LoggerFactory.getLogger(VideoController.class);

  private VideoService videoService;

  @Autowired
  public VideoController(VideoService videoService) {
    this.videoService = videoService;
  }

  @RequestMapping(value = "/video", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> addVideo(@RequestBody Video video) {
    String videoUid = videoService.addVideo(video);
    JSONObject responseJson = new JSONObject().put("videoUid", videoUid);
    return new ResponseEntity<>(responseJson.toString(), HttpStatus.OK);
  }

  @RequestMapping(value = "/video", method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> updateVideo(@RequestBody Video video) {
    videoService.updateVideo(video);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

  @RequestMapping(value = "/video/{deviceUid}/{videoUid}", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Video> getVideo(
      @PathVariable("deviceUid") String deviceUid,
      @PathVariable("videoUid") String videoUid) {
    Video video = videoService.getVideo(videoUid, deviceUid);
    return new ResponseEntity<>(video, HttpStatus.OK);
  }

  @RequestMapping(value = "/video/{deviceUid}", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Video>> getVideo(@PathVariable("deviceUid") String deviceUid) {
    List<Video> videos = videoService.getVideos(deviceUid);
    return new ResponseEntity<>(videos, HttpStatus.OK);
  }

  @RequestMapping(value = "/video/{deviceUid}/{videoUid}", method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteVideo(
      @PathVariable("deviceUid") String deviceUid,
      @PathVariable("videoUid") String videoUid) {
    videoService.deleteVideo(videoUid, deviceUid);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

}

package com.cmpe295.iot.admin.rest;

import com.cmpe295.iot.admin.model.ClientInfo;
import com.cmpe295.iot.admin.service.ClientInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientInfoController {

  private ClientInfoService clientInfoService;

  @Autowired
  public ClientInfoController(ClientInfoService clientInfoService) {
    this.clientInfoService = clientInfoService;
  }

  @RequestMapping(value = "/client",
    method = RequestMethod.PUT,
    consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> upsertClientInfo(@RequestBody ClientInfo clientInfo) {
    clientInfoService.upsertClientInfo(clientInfo);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = "/client/{clientUid}",
      method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteClientInfo(@PathVariable("clientUid") String clientUid) {
    clientInfoService.deleteClientInfo(clientUid);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

}

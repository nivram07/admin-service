package com.cmpe295.iot.admin.rest;

import com.cmpe295.iot.admin.model.Device;
import com.cmpe295.iot.admin.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DeviceController {

  private DeviceService deviceService;

  @Autowired
  public DeviceController(DeviceService deviceService) {
    this.deviceService = deviceService;
  }

  @RequestMapping(value = "/device",
      method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> upsertDevice(
      @RequestBody Device device) {
    deviceService.upsertClientDevice(device);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = "/device/{deviceUid}",
      method = RequestMethod.DELETE)
  public ResponseEntity<Void> deleteDevice(@PathVariable("deviceUid") String deviceUid) {
    deviceService.deleteClientDevice(deviceUid);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
}

package com.cmpe295.iot.admin.repository;

import com.cmpe295.iot.admin.constant.AdminConstants;
import com.cmpe295.iot.admin.constant.VideoConstants;
import com.cmpe295.iot.admin.exception.SystemErrorException;
import com.cmpe295.iot.admin.model.Video;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Repository
public class VideoRepository implements CustomRepository<Video, String> {

  private static final Logger LOGGER = LoggerFactory.getLogger(VideoRepository.class);

  private Firestore firestore;

  public VideoRepository(Firestore firestore) {
    this.firestore = firestore;
  }

  @Override
  public void upsert(Video video) {
    ApiFuture<WriteResult> future = firestore.collection(VideoConstants.VIDEO_COLLECTION_NAME)
        .document(video.getDeviceUid())
        .collection(AdminConstants.FILES_PATH)
        .document(video.getVideoUid()).set(video);

    try {
      LOGGER.info("Update time for video: {} is {}", video.toString(), future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }

  }

  public Video findByKey(String videoUid, String deviceUid) {
    ApiFuture<DocumentSnapshot> future = firestore.collection(VideoConstants.VIDEO_COLLECTION_NAME)
        .document(deviceUid)
        .collection(AdminConstants.FILES_PATH)
        .document(videoUid).get();
    try {
      DocumentSnapshot documentSnapshot = future.get();
      return documentSnapshot.toObject(Video.class);
    } catch (InterruptedException intEx) {
      LOGGER.error("Interrupted exception occurred. {}", intEx);
      throw new SystemErrorException("document retrieval was interrupted with key " + videoUid);
    } catch (ExecutionException ex) {
      LOGGER.error("Execution exception occurred. {}", ex);
      throw new SystemErrorException("There was a problem executing document retrieval with key " + videoUid);
    }
  }

  @Override
  public void delete(String videoUid, String deviceUid) {
    ApiFuture<WriteResult> future = firestore.collection(VideoConstants.VIDEO_COLLECTION_NAME)
        .document(deviceUid)
        .collection(AdminConstants.FILES_PATH)
        .document(videoUid).delete();

    try {
      LOGGER.info("Delete time for video: {} is {}", videoUid, future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }

  }

  @Override
  public List<Video> findByParentKey(String deviceUid) {
    ApiFuture<QuerySnapshot> future = firestore.collection(VideoConstants.VIDEO_COLLECTION_NAME)
        .document(deviceUid)
        .collection(AdminConstants.FILES_PATH)
        .orderBy("timestamp", Query.Direction.DESCENDING)
        .get();

    try {
      List<QueryDocumentSnapshot> documents = future.get().getDocuments();
      return documents.parallelStream().map(o -> o.toObject(Video.class)).collect(Collectors.toList());
    } catch (InterruptedException intEx) {
      LOGGER.error("Interrupted exception occurred. {}", intEx);
      throw new SystemErrorException("Documents retrieval was interrupted with key " + deviceUid);
    } catch (ExecutionException ex) {
      LOGGER.error("Execution exception occurred. {}", ex);
      throw new SystemErrorException("There was a problem executing document retrieval with key " + deviceUid);
    }
  }
}

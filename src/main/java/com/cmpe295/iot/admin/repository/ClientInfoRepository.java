package com.cmpe295.iot.admin.repository;

import com.cmpe295.iot.admin.constant.ClientConstants;
import com.cmpe295.iot.admin.exception.SystemErrorException;
import com.cmpe295.iot.admin.model.ClientInfo;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ExecutionException;


@Repository
public class ClientInfoRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientInfoRepository.class);

  private Firestore firestore;

  @Autowired
  public ClientInfoRepository(Firestore firestore) {
    this.firestore = firestore;
  }

  public void upsert(ClientInfo clientInfo) {
    ApiFuture<WriteResult> future = firestore.collection(ClientConstants.CLIENT_COLLECTION_NAME)
        .document(clientInfo.getClientUid()).set(clientInfo);

    try {
      LOGGER.info("Update time for client: {} is {}", clientInfo.toString(), future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }

  }

  public ClientInfo findByKey(String clientUid) {
    ApiFuture<DocumentSnapshot> future = firestore.collection(ClientConstants.CLIENT_COLLECTION_NAME)
        .document(clientUid).get();
    try {
      DocumentSnapshot documentSnapshot = future.get();
      return documentSnapshot.toObject(ClientInfo.class);
    } catch (InterruptedException intEx) {
      LOGGER.error("Interrupted exception occurred. {}", intEx);
      throw new SystemErrorException("document retrieval was interrupted with key " + clientUid);
    } catch (ExecutionException ex) {
      LOGGER.error("Execution exception occurred. {}", ex);
      throw new SystemErrorException("There was a problem executing document retrieval with key " + clientUid);
    }
  }


  public void delete(String clientUid) {
    ApiFuture<WriteResult> future = firestore.collection(ClientConstants.CLIENT_COLLECTION_NAME)
        .document(clientUid).delete();

    try {
      LOGGER.info("Delete time for clientInfo: {} is {}", clientUid, future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }
  }

}

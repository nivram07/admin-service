package com.cmpe295.iot.admin.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Custom repository for simple Firestore data structure,
 * each object will have a parentUid that will be used as its parent 'directory'
 * @param <T>
 * @param <K>
 */
public interface CustomRepository<T, K extends Serializable> {

  void upsert(T object);

  T findByKey(K key, String parentKey);

  List<T> findByParentKey(String parentKey);

  void delete(K key, String parentKey);
}

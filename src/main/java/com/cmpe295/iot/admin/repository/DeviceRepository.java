package com.cmpe295.iot.admin.repository;

import com.cmpe295.iot.admin.constant.DeviceConstants;
import com.cmpe295.iot.admin.exception.SystemErrorException;
import com.cmpe295.iot.admin.model.Device;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.concurrent.ExecutionException;

@Repository
public class DeviceRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(DeviceRepository.class);

  private Firestore firestore;

  @Autowired
  public DeviceRepository(Firestore firestore) {
    this.firestore = firestore;
  }

  public void upsert(Device device) {
    ApiFuture<WriteResult> future = firestore.collection(DeviceConstants.DEVICE_COLLECTION_NAME)
        .document(device.getDeviceUid())
        .set(device);

    try {
      LOGGER.info("Update time for device: {} is {}", device.toString(), future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }

  }

  public Device findByKey(String deviceUid) {
    ApiFuture<DocumentSnapshot> future = firestore.collection(DeviceConstants.DEVICE_COLLECTION_NAME)
        .document(deviceUid).get();
    try {
      DocumentSnapshot documentSnapshot = future.get();
      return documentSnapshot.toObject(Device.class);
    } catch (InterruptedException intEx) {
      LOGGER.error("Interrupted exception occurred. {}", intEx);
      throw new SystemErrorException("document retrieval was interrupted with key " + deviceUid);
    } catch (ExecutionException ex) {
      LOGGER.error("Execution exception occurred. {}", ex);
      throw new SystemErrorException("There was a problem executing document retrieval with key " + deviceUid);
    }
  }


  public void delete(String deviceUid) {
    ApiFuture<WriteResult> future = firestore.collection(DeviceConstants.DEVICE_COLLECTION_NAME)
        .document(deviceUid).delete();

    try {
      LOGGER.info("Delete time for clientDevice: {} is {}", deviceUid, future.get().getUpdateTime());
    } catch (Exception ex) {
      LOGGER.error("Unable to get future update time. {}", ex);
    }
  }
}

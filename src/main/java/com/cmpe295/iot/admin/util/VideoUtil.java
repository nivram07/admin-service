package com.cmpe295.iot.admin.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoUtil {

  private static final Logger LOGGER = LoggerFactory.getLogger(VideoUtil.class);

  private static final String GET_PUBLIC_ID_PATTERN = "(.*\\/)(.+)(\\..+)";
  /**
   * Extract public id from the url.
   * @param url ex. http://res.cloudinary.com/sar247/video/upload/v1517986042/user01_vid_02072018_064722.m3u8
   * @return user01_vid_02072018_064722
   */
  public static String getVideoPublicId(String url) {
    Pattern pattern = Pattern.compile(GET_PUBLIC_ID_PATTERN);
    Matcher matcher = pattern.matcher(url);
    String videoPublicId = null;
    if (matcher.matches()) {
      videoPublicId = matcher.group(2);
      LOGGER.info("VIDEO PUBLIC ID: {}", videoPublicId);
    }
    return videoPublicId;
  }
}

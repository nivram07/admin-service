package com.cmpe295.iot.admin.exception;

public class VideoNotFoundException extends RuntimeException {
  public VideoNotFoundException(String message) {
    super(message);
  }
}

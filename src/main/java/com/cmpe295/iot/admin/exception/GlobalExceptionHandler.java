package com.cmpe295.iot.admin.exception;

import com.cmpe295.iot.admin.model.ExceptionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class GlobalExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  @ExceptionHandler( value = {VideoNotFoundException.class})
  public ResponseEntity<ExceptionMessage> notFoundException(RuntimeException ex, WebRequest request) {
    ExceptionMessage message = new ExceptionMessage();
    message.setMessage(ex.getMessage());
    message.setTimestamp(new Date());
    return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler( value = {InvalidInputException.class})
  public ResponseEntity<ExceptionMessage> invalidInputException(RuntimeException ex) {
    ExceptionMessage message = new ExceptionMessage();
    message.setMessage(ex.getMessage());
    message.setTimestamp(new Date());
    return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
  }


}

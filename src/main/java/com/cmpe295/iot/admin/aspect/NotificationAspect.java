package com.cmpe295.iot.admin.aspect;

import com.cmpe295.iot.admin.model.ClientInfo;
import com.cmpe295.iot.admin.model.Device;
import com.cmpe295.iot.admin.model.Video;
import com.cmpe295.iot.admin.service.ClientInfoService;
import com.cmpe295.iot.admin.service.DeviceService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NotificationAspect {

  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationAspect.class);

  private DeviceService deviceService;

  private ClientInfoService clientInfoService;

  @Autowired
  public NotificationAspect(DeviceService deviceService, ClientInfoService clientInfoService) {
    this.deviceService = deviceService;
    this.clientInfoService = clientInfoService;
  }

  @After("@annotation(com.cmpe295.iot.admin.annotation.SendNotification)")
  public void sendNotification(JoinPoint joinPoint) {
      LOGGER.info("Sending notification for: {}", joinPoint.getArgs()[0].toString());
      if (joinPoint.getArgs() != null && joinPoint.getArgs().length > 0) {
        for (Object object : joinPoint.getArgs()) {
          if (object instanceof Video) {
            Video video = (Video) object;
            Device device = deviceService.getDeviceByUid(video.getDeviceUid());
            ClientInfo clientInfo = clientInfoService.getClientInfoByUid(device.getClientUid());
            if (clientInfo != null && clientInfo.getAppToken() != null) {
              sendData(clientInfo.getAppToken(), video);
            }
          }
        }
      }
  }

  private void sendData(String token, Video video) {
    Notification notification = new Notification("New Video", "A new Video has been received!");
    Message message = Message.builder()
        .putData("timestamp", video.getTimestamp().toString())
        .setNotification(notification)
        .setToken(token) // token must not be null
        .build();

    try {
      // Send a message to the device corresponding to the provided
      // registration token.
      String response = FirebaseMessaging.getInstance().send(message);

      // Response is a message ID string.
      LOGGER.info("Successfully sent message: " + response);
    } catch (FirebaseMessagingException ex) {
      LOGGER.error("An error occurred while trying to send to message {}", ex.toString());
    }
  }

}

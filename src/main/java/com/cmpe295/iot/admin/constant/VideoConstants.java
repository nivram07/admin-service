package com.cmpe295.iot.admin.constant;

/**
 * Class that holds all the constants regarding video metadata
 */
public class VideoConstants {

  public static final String VIDEO_COLLECTION_NAME = "video";
}

package com.cmpe295.iot.admin.constant;

public class AdminConstants {

  // used as path name in Firestore
  public final static String FILES_PATH = "files";
}

package com.cmpe295.iot.admin.service;

import com.cmpe295.iot.admin.model.ClientInfo;
import com.cmpe295.iot.admin.repository.ClientInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientInfoService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientInfoService.class);

  private ClientInfoRepository clientInfoRepository;

  @Autowired
  public ClientInfoService(ClientInfoRepository clientInfoRepository) {
    this.clientInfoRepository = clientInfoRepository;
  }

  public ClientInfo getClientInfoByUid(String clientUid) {
    return clientInfoRepository.findByKey(clientUid);
  }

  public void upsertClientInfo(ClientInfo clientInfo) {
    clientInfoRepository.upsert(clientInfo);
  }

  public void deleteClientInfo(String clientUid) {
    clientInfoRepository.delete(clientUid);
  }
}

package com.cmpe295.iot.admin.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.api.ApiResponse;
import com.cloudinary.utils.ObjectUtils;
import com.cmpe295.iot.admin.annotation.SendNotification;
import com.cmpe295.iot.admin.exception.InvalidInputException;
import com.cmpe295.iot.admin.exception.VideoNotFoundException;
import com.cmpe295.iot.admin.model.Video;
import com.cmpe295.iot.admin.repository.CustomRepository;
import com.cmpe295.iot.admin.util.VideoUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class VideoService {

  private static final Logger LOGGER = LoggerFactory.getLogger(VideoService.class);

  private CustomRepository<Video, String> videoRepository;

  private Cloudinary cloudinary;


  @Autowired
  public VideoService(CustomRepository<Video, String> videoRepository,
                      Cloudinary cloudinary) {
    this.videoRepository = videoRepository;
    this.cloudinary = cloudinary;
  }

  @SendNotification
  public String addVideo(Video video) {
    String videoUid = UUID.randomUUID().toString();
    video.setVideoUid(videoUid);
    video.setIsNew(true);
    videoRepository.upsert(video);
    return videoUid;
  }

  public void updateVideo(Video video) {
    if (StringUtils.isBlank(video.getDeviceUid())) {
      throw new InvalidInputException("Invalid videoUid.");
    }
    videoRepository.upsert(video);
  }

  public Video getVideo(String videoUid, String deviceUid) {
    Video video = videoRepository.findByKey(videoUid, deviceUid);
    if (video == null) {
      throw new VideoNotFoundException("Unable to find video with id " + videoUid);
    }
    return video;
  }

  public List<Video> getVideos(String deviceUid) {
    return videoRepository.findByParentKey(deviceUid);
  }

  public void deleteVideo(String videoUid, String deviceUid) {
    Video video = videoRepository.findByKey(videoUid, deviceUid);
    if (video != null) {
      String videoPublicId = VideoUtil.getVideoPublicId(video.getUrl());
      try {
        ApiResponse response = cloudinary.api().deleteResources(Collections.singletonList(videoPublicId), ObjectUtils.asMap("resource_type", "video"));
        LOGGER.info("Cloudinary response: {}", response);
        videoRepository.delete(videoUid, deviceUid);
      } catch (Exception ex) {
        LOGGER.error("Unable to delete resource: {}, {}", videoPublicId, ex);

      }
    }
  }
}

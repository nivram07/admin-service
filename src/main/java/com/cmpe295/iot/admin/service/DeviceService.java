package com.cmpe295.iot.admin.service;

import com.cmpe295.iot.admin.model.Device;
import com.cmpe295.iot.admin.repository.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {

  private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);

  private DeviceRepository deviceRepository;

  @Autowired
  public DeviceService(DeviceRepository deviceRepository) {
    this.deviceRepository = deviceRepository;
  }

  public Device getDeviceByUid(String deviceUid) {
    return deviceRepository.findByKey(deviceUid);
  }

  public void upsertClientDevice(Device device) {
    deviceRepository.upsert(device);
  }

  public void deleteClientDevice(String deviceUid) {
    deviceRepository.delete(deviceUid);
  }

}

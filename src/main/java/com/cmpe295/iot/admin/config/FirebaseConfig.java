package com.cmpe295.iot.admin.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.FileInputStream;
import java.io.IOException;

@Configuration
public class FirebaseConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirebaseConfig.class);

    private EnvConfig envConfig;

    @Autowired
    public FirebaseConfig(EnvConfig envConfig) {
        this.envConfig = envConfig;
    }

    @Bean
    @Profile("dev")
    public FirebaseOptions firebaseOptions() throws IOException {
        FileInputStream credentialsInputStream = new FileInputStream(envConfig.getCredentials().getAdminSdk());

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(credentialsInputStream))
                .build();

        return options;
    }

    @Bean
    @Profile("!dev")
    public FirebaseOptions cloudFirebaseOptions() throws IOException {
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.getApplicationDefault())
                .setDatabaseUrl("https://<DATABASE_NAME>.firebaseio.com/")
                .build();
        return options;
    }

    @Bean
    public Firestore firestore(FirebaseOptions options) {
        LOGGER.info("Initializing Firebase...");
        FirebaseApp.initializeApp(options);
        return FirestoreClient.getFirestore();
    }
}

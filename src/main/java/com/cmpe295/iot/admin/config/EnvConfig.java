package com.cmpe295.iot.admin.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class EnvConfig {

    private Credentials credentials = new Credentials();
    private Services services = new Services();

    @Data
    public class Credentials {
        private String adminSdk;
    }

    @Data
    public class Services {
        private MediaService mediaService = new MediaService();
        private Cloudinary cloudinary = new Cloudinary();

        @Data
        public class MediaService {
            private String url;
        }

        @Data
        public class Cloudinary {
            private String apiKey;
            private String apiSecret;
            private String cloudName;
        }
    }

}

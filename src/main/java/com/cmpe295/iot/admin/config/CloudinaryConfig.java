package com.cmpe295.iot.admin.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudinaryConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(CloudinaryConfig.class);


  private EnvConfig envConfig;

  @Autowired
  public CloudinaryConfig(EnvConfig envConfig) {
    this.envConfig = envConfig;
  }

  @Bean
  public Cloudinary cloudinary() {
    LOGGER.info("Cloud name: {}", envConfig.getServices().getCloudinary().getCloudName());
    Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
        "cloud_name", envConfig.getServices().getCloudinary().getCloudName(),
        "api_key", envConfig.getServices().getCloudinary().getApiKey(),
        "api_secret", envConfig.getServices().getCloudinary().getApiSecret()));
    return  cloudinary;
  }
}

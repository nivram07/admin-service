package com.cmpe295.iot.admin.model;

import lombok.Data;

import java.util.Date;

/**
 * Class that represents video metadata.
 */
@Data
public class Video {

  private String videoUid;
  private String deviceUid;
  private Date timestamp;
  private String url;

  // This is set to true once a new Video has been uploaded. The mobile
  // application is responsible to update this.
  private Boolean isNew;

}

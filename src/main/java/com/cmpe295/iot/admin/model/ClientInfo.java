package com.cmpe295.iot.admin.model;

import lombok.Data;

@Data
public class ClientInfo {
  private String clientUid;
  private String email;
  private String appToken;
}

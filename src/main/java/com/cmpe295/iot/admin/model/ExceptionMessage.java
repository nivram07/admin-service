package com.cmpe295.iot.admin.model;

import lombok.Data;

import java.util.Date;

@Data
public class ExceptionMessage {
  private String message;
  private Date timestamp;
}

package com.cmpe295.iot.admin.model;

import lombok.Data;

@Data
public class Device {
  private String deviceUid;
  private String clientUid;
  private String name;
  private String macAddress;
  private String description;
}
